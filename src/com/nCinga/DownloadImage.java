package com.nCinga;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class DownloadImage implements Callable<Integer> {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        List<String> urlList = new ArrayList<String>();

        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/baboon.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/boat.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/cat.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/frymire.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/girl.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/monarch.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/peppers.png");


        int length = urlList.size();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(length);

        List<Future<Integer>> resultList = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            DownloadImage downloadImage = new DownloadImage(urlList.get(i), "image" + (i + 1));
            Future<Integer> result = executor.submit(downloadImage);
            resultList.add(result);
        }

        for (int j = 0; j < length; j++) {
            Future<Integer> future = resultList.get(j);

            if (future.get() == 0) {
                System.out.println("Downloads are not completed");
                break;
            } else if (j == length - 1 && future.get() == 1)
                System.out.println("Downloads are completed");

        }

        awaitTerminationAfterShutdown(executor);

    }

    private final String url;
    private final String imgName;

    public DownloadImage(String url, String imgName) {
        this.url = url;
        this.imgName = imgName;
    }


    @Override
    public Integer call() {

        URL url = null;
        try {
            url = new URL(this.url);
        } catch (MalformedURLException e) {
            System.out.println("To download " + this.imgName + " " + e.getMessage());
            return 0;
        }
        BufferedImage img = null;
        try {
            img = ImageIO.read(url);
            File file = new File("D:\\Down_Img\\" + this.imgName + ".png");
            ImageIO.write(img, "png", file);
            return 1;
        } catch (IOException e) {
            System.out.println("To download " + this.imgName + " " + e.getMessage());
            return 0;
        }

    }

    public static void awaitTerminationAfterShutdown(ThreadPoolExecutor threadPool) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}


