package com.nCinga;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Download implements Runnable {

    public static void main(String[] args) throws InterruptedException {

        List<String> urlList = new ArrayList<String>();

        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/baboon.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/boat.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/cat.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/frymire.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/girl.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/monarch.png");
        urlList.add("https://homepages.cae.wisc.edu/~ece533/images/peppers.png");

        int length = urlList.size();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(length);

        for (int i = 0; i < length; i++) {
            Download downloadImage = new Download(urlList.get(i), "image" + (i + 1));
            executor.execute(downloadImage);
        }

        awaitTerminationAfterShutdown(executor);

    }

    private final String url;
    private final String imgName;

    public Download(String url, String imgName) {
        this.url = url;
        this.imgName = imgName;
    }

    @Override
    public void run() {
        URL url = null;
        try {
            url = new URL(this.url);
        } catch (MalformedURLException e) {
            System.out.println("To download " + this.imgName + " " + e.getMessage());
        }
        BufferedImage img = null;
        try {
            img = ImageIO.read(url);
            File file = new File("D:\\Down_Img\\" + this.imgName + ".png");
            ImageIO.write(img, "png", file);
            System.out.println(this.imgName + " Downloaded");
        } catch (IOException e) {
            System.out.println("To download " + this.imgName + " " + e.getMessage());
        }

    }

    public static void awaitTerminationAfterShutdown(ThreadPoolExecutor threadPool) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
